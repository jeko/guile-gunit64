;;    This file is part of guile-gunit64.
;;
;;    "guile-gunit64" is free software: you can redistribute it and/or modify
;;    it under the terms of the GNU General Public License as published by
;;    the Free Software Foundation, either version 3 of the License, or
;;    (at your option) any later version.
;;
;;    "guile-gunit64" is distributed in the hope that it will be useful,
;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;    GNU General Public License for more details.
;;
;;    You should have received a copy of the GNU General Public License
;;    along with "guile-gunit64".  If not, see <https://www.gnu.org/licenses/>

(define-module (gunit64)
  #:use-module ((srfi srfi-64))
  #:use-module (ice-9 format)
  #:use-module (srfi srfi-26)
  #:use-module (ice-9 pretty-print)
  #:use-module (ice-9 match)
  #:use-module (ice-9 regex)
  #:export (*test-root*
	    *quiet-runner*
	    reset-gunit64
	    run-tests
	    run-root-tests
	    run-only-tests
	    run-failed-tests
	    re-run-tests
	    ))

(re-export
 ;; test-begin
 ;; test-end
 test-assert
 ;; test-eqv
 ;; test-eq
 test-equal
 ;; test-approximate
 ;; test-assert
 test-error
 ;; test-apply
 ;; test-with-runner
 ;; test-match-nth
 ;; test-match-all
 ;; test-match-any
 ;; test-match-name
 ;; test-skip
 ;; test-expect-fail
 ;; test-read-eval-string
 ;; test-runner-group-path
 test-group
 test-group-with-cleanup
 ;; test-result-ref
 ;; test-result-set!
 ;; test-result-clear
 ;; test-result-remove
 ;; test-result-kind
 ;; test-passed?
 ;; test-log-to-file
 ;; test-runner?
 ;; test-runner-reset
 ;; test-runner-null
 ;; test-runner-simple
 ;; test-runner-current
 ;; test-runner-factory
 ;; test-runner-get
 ;; test-runner-create
 ;; test-runner-test-name
 ;; test-runner-pass-count
 ;; test-runner-pass-count!
 ;; test-runner-fail-count
 ;; test-runner-fail-count!
 ;; test-runner-xpass-count
 ;; test-runner-xpass-count!
 ;; test-runner-xfail-count
 ;; test-runner-xfail-count!
 ;; test-runner-skip-count
 ;; test-runner-skip-count!
 ;; test-runner-group-stack
 ;; test-runner-group-stack!
 ;; test-runner-on-test-begin
 ;; test-runner-on-test-begin!
 ;; test-runner-on-test-end
 ;; test-runner-on-test-end!
 ;; test-runner-on-group-begin
 ;; test-runner-on-group-begin!
 ;; test-runner-on-group-end
 ;; test-runner-on-group-end!
 ;; test-runner-on-final
 ;; test-runner-on-final!
 ;; test-runner-on-bad-count
 ;; test-runner-on-bad-count!
 ;; test-runner-on-bad-end-name
 ;; test-runner-on-bad-end-name!
 ;; test-result-alist
 ;; test-result-alist!
 ;; test-runner-aux-value
 ;; test-runner-aux-value!
 ;; test-on-group-begin-simple
 ;; test-on-group-end-simple
 ;; test-on-bad-count-simple
 ;; test-on-bad-end-name-simple
 ;; test-on-final-simple
 ;; test-on-test-end-simple
 ;; test-on-final-simple
 )

(define *test-root* (make-parameter ""))
(define *quiet-runner* (make-parameter #f))

(define %executed-tests '())
(define %failed-tests '())
(define %execution-time #f)

(define (reset-executed-tests) (set! %executed-tests '()))
(define (reset-failed-tests) (set! %failed-tests '()))
(define (reset-execution-time) (set! %execution-time #f))

(define (reset-gunit64)
  (reset-executed-tests)
  (reset-failed-tests)
  (reset-execution-time))


(define (gunit64-runner)

  (define preffix-space 4)

  (define (on-group-begin runner suite-name count)
    (set! %execution-time (current-time)))

  (define (on-group-end runner)
    (unless (*quiet-runner*)
      (format #t "\n~v_> ~A\n\n"
	      (/ preffix-space 2)
	      (strftime "%c" (localtime %execution-time)))))

  
  (define (on-test-end runner)

    (define* (result->string symbol)
      (let ((result (string-upcase (symbol->string symbol))))
	(string-append (case symbol
			 ((pass)       "[0;32m")  ;green
			 ((xfail)      "[1;32m")  ;light green
			 ((skip)       "[0;34m")  ;blue
			 ((fail xpass) "[1;31m")  ;red
			 ((error)      "[0;35m")) ;magenta
                       result
                       "[m")))
    
    (define* (test-display field value #:key error?)
      (if error?
	  (begin
            (format #t "~v_~A:~%"
		    preffix-space
		    field)
            (pretty-print value #:per-line-prefix "+ "))
	  (format #t "~v_~A: ~S~%"
		  (1+ (* 2 preffix-space))
		  field
		  value)))

    (let* ((results (test-result-alist runner))
	   (result? (cut assq <> results))
	   (result  (cut assq-ref results <>)))
      (cond
       ((eq? 'fail (result 'result-kind))
	(begin
	  (set! %executed-tests (cons (test-runner-test-name runner) %executed-tests))
	  (set! %failed-tests (cons (result 'test-name) %failed-tests))
	  (unless (*quiet-runner*)
	    (begin
	      (format #t "~v_~a ~A~%"
		      preffix-space
		      (result->string (result 'result-kind))
		      (result 'test-name))
	      (when (result? 'expected-value)
		(test-display "expected-value" (result 'expected-value)))
	      (when (result? 'expected-error)
		(test-display "expected-error" (result 'expected-error) #:error? #t))
	      (when (result? 'actual-value)
		(test-display "actual-value" (result 'actual-value)))
	      (when (result? 'actual-error)
		(test-display "actual-error" (result 'actual-error) #:error? #t))
	      (format #t "~v_~A:~A~%"
		      (1+ (* 2 preffix-space))
		      (result 'source-file)
		      (result 'source-line))))))
       ((eq? 'skip (result 'result-kind)) (unless (*quiet-runner*) ""))
       (else
	(begin
	  (set! %executed-tests (cons (test-runner-test-name runner) %executed-tests))
	  (unless (*quiet-runner*)
	    (format #t "~v_~a ~A~%"
		    preffix-space
		    (result->string (result 'result-kind))
		    (result 'test-name))))))))  
  
  (let ((runner (test-runner-null)))
    (test-runner-on-test-end! runner on-test-end)
    (test-runner-on-group-begin! runner on-group-begin)
    (test-runner-on-group-end! runner on-group-end)
    runner))

(define* (run-tests #:key rerun failed only)

  (define (test-match-name* regexp)
    (lambda (runner)
      (string-match regexp (test-runner-test-name runner))))

  (define (test-match-name*/negated regexp)
    (lambda (runner)
      (not (string-match regexp (test-runner-test-name runner)))))

  (define (list->regex words)
    (format #f "^(~a)$" (string-join words "|")))

  (lambda (tests)
    (let ([last-executed-tests (list->regex %executed-tests)]
	  [last-failed-tests (list->regex %failed-tests)])
      (reset-gunit64)
      (test-with-runner (gunit64-runner)
	(cond (rerun
	       (when last-executed-tests
		 (test-skip (test-match-name*/negated last-executed-tests))))
	      (failed
	       (when last-failed-tests
		 (test-skip (test-match-name*/negated last-failed-tests))))
	      (only
	       (test-skip (test-match-name*/negated (list->regex only)))))
	(test-begin "HARNESS")
	(tests)
	(test-end "HARNESS")))))

(define (run-root-tests)
  ((run-tests) (*test-root*)))

(define (run-only-tests tests)
  ((run-tests #:only tests) (*test-root*)))

(define (run-failed-tests)
  ((run-tests #:failed #t) (*test-root*)))

(define (re-run-tests)
  ((run-tests #:rerun #t) (*test-root*)))
