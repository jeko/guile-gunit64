((scheme-mode . (;; Macro indentation
		 ;; note: (eval . (put 'my-syntax 'scheme-indent-function 1)) 
		 ;; Add test runner report colorization
		 (eval . (setq geiser-debug-treat-ansi-colors 'colors))
		 ;; Do not jump to *Geiser Debug* when compil error
		 (eval . (setq geiser-debug-jump-to-debug nil)))))
