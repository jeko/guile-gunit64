;;    This file is part of guile-gunit64.
;;
;;    "guile-gunit64" is free software: you can redistribute it and/or modify
;;    it under the terms of the GNU General Public License as published by
;;    the Free Software Foundation, either version 3 of the License, or
;;    (at your option) any later version.
;;
;;    "guile-gunit64" is distributed in the hope that it will be useful,
;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;    GNU General Public License for more details.
;;
;;    You should have received a copy of the GNU General Public License
;;    along with "guile-gunit64".  If not, see <https://www.gnu.org/licenses/>

(use-modules
 (jeko-packages)
 (guix packages)
 (guix gexp)
 (guix git-download)
 (ice-9 popen)
 (ice-9 rdelim))

(define source-dir (dirname (current-filename)))

(package
  (inherit guile-gunit64)
  (name "guile-gunit64-git")
  (version (string-append (package-version guile-gunit64) "-HEAD"))
  (source (local-file source-dir #:recursive? #t)))

