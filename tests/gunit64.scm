;;    This file is part of guile-gunit64.
;;
;;    "guile-gunit64" is free software: you can redistribute it and/or modify
;;    it under the terms of the GNU General Public License as published by
;;    the Free Software Foundation, either version 3 of the License, or
;;    (at your option) any later version.
;;
;;    "guile-gunit64" is distributed in the hope that it will be useful,
;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;    GNU General Public License for more details.
;;
;;    You should have received a copy of the GNU General Public License
;;    along with "guile-gunit64".  If not, see <https://www.gnu.org/licenses/>

(use-modules (gunit64))

(*test-root*
 (lambda ()
   
   (test-equal "one-executed-test-should-be-recorded"
     '("to-execute")
     (begin
       (reset-gunit64)
       (parameterize ((*quiet-runner* #t))
	 ((run-tests)
	  (lambda () (test-assert "to-execute" #f))))
       (@@ (gunit64) %executed-tests)))

   (test-equal "two-executed-tests-should-be-recorded"
     '("to-execute-2" "to-execute-1")
     (begin
       (reset-gunit64)
       (parameterize ((*quiet-runner* #t))
	 ((run-tests)
	  (lambda ()
	    (test-assert "to-execute-1" #f)
	    (test-assert "to-execute-2" #t))))
       (@@ (gunit64) %executed-tests)))

   (test-equal "run-all-recorded-tests"
     '("to-execute-3" "to-execute-2" "to-execute-1")
     (begin
       (reset-gunit64)
       (parameterize ((*quiet-runner* #t))
	 ((run-tests)
	  (lambda ()
	    (test-assert "to-execute-1" #f)
	    (test-assert "to-execute-2" #t)
	    (test-assert "to-execute-3" #f))))
       (@@ (gunit64) %executed-tests)))

   (test-equal "rerun-only-executed-tests"
     '("to-execute-5" "to-execute-4")
     (begin
       (reset-gunit64)
       (set! (@@ (gunit64) %executed-tests) '("to-execute-5" "to-execute-4"))
       (parameterize ((*quiet-runner* #t))	 
	 ((run-tests #:rerun #t)
	  (lambda ()
	    (test-assert "to-skip-1" #f)
	    (test-assert "to-skip-2" #t)
	    (test-assert "to-skip-3" #t)
	    (test-assert "to-execute-4" #t)
	    (test-assert "to-execute-5" #f))))
       (@@ (gunit64) %executed-tests)))

   (test-equal "test-names-should-match-strictly"
     '("to-execute")
     (begin
       (reset-gunit64)
       (set! (@@ (gunit64) %executed-tests) '("to-execute"))
       (parameterize ((*quiet-runner* #t))
	 ((run-tests #:rerun #t)
	  (lambda ()
	    (test-assert "to-execute-not-strict" #f)
	    (test-assert "to-execute" #f))))
       (@@ (gunit64) %executed-tests)))

   (test-equal "rerun-only-failed-tests"
     '("to-execute-4" "to-execute-2")
     (begin
       (reset-gunit64)
       (set! (@@ (gunit64) %failed-tests) '("to-execute-2" "to-execute-4"))
       (parameterize ((*quiet-runner* #t))	 
	 ((run-tests #:failed #t)
	  (lambda ()
	    (test-assert "to-skip-1" #f)
	    (test-assert "to-execute-2" #t)
	    (test-assert "to-skip-3" #t)
	    (test-assert "to-execute-4" #f)
	    (test-assert "to-skip-5" #f))))
       (@@ (gunit64) %executed-tests)))

   (test-equal "rerun-only-given-tests"
     '("to-execute-5" "to-execute-3" "to-execute-1")
     (begin
       (reset-gunit64)
       (parameterize ((*quiet-runner* #t))	 
	 ((run-tests #:only '("to-execute-5"
			      "to-execute-3"
			      "to-execute-1"))
	  (lambda ()
	    (test-assert "to-execute-1" #f)
	    (test-assert "to-skip-2" #t)
	    (test-assert "to-execute-3" #t)
	    (test-assert "to-skip-4" #f)
	    (test-assert "to-execute-5" #f))))
       (@@ (gunit64) %executed-tests)))

   (test-assert "capture-date-and-time-of-execution"
     (begin
       (reset-gunit64)
       (parameterize ((*quiet-runner* #t))	 
	 ((run-tests) (lambda () (test-assert "to-execute-1" #f))))
       (number? (@@ (gunit64) %execution-time))))))

((run-tests) (*test-root*))


;; TODO
;;;; skip group test (https://srfi.schemers.org/srfi-64/mail-archive/msg00013.html)
;;;; random test execution order
;;;; run module tests
;;;; run project tests

